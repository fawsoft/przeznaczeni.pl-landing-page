var currSlide = 0; 
var slideDelay = 500;
var slideInterval = 15000;
var isSlide = true;
$(document).ready(function(){ 
	$('.slider-left').on('click', function(e){
		e.preventDefault();
		isSlide = false;
		$('.slider-'+currSlide).fadeOut(slideDelay, function(){
			$('.slider').hide();
			currSlide -= 1;
			if(currSlide<0) currSlide = 2;
			$('.slider-'+currSlide).fadeIn(slideDelay);
			$('.point').removeClass('selected');
			$('.point-'+currSlide).addClass('selected');
		}); 
	});
	$('.slider-right').on('click', function(e){
		e.preventDefault();
		isSlide = false;
		$('.slider-'+currSlide).fadeOut(slideDelay, function(){
			$('.slider').hide();
			currSlide += 1;
			if(currSlide>2) currSlide = 0;
			$('.slider-'+currSlide).fadeIn(slideDelay);
			$('.point').removeClass('selected');
			$('.point-'+currSlide).addClass('selected');
		});
	});
	$('.point').on('click', function(){
		isSlide = false;
		$this = $(this);
		$('.slider-'+currSlide).fadeOut(slideDelay, function(){
			$('.slider').hide();
			currSlide = 1*$this.data('point');
			$('.slider-'+currSlide).fadeIn(slideDelay);
			$('.point').removeClass('selected');
			$('.point-'+currSlide).addClass('selected');
		});
	});
	detectAndroid();
});

function detectAndroid(){
	var ua = navigator.userAgent.toLowerCase();
	var isAndroid = ua.indexOf("android") > -1; //&& ua.indexOf("mobile");
	if(isAndroid) {
		$('.hide-android').hide();
		$('.show-android').css({display: 'inline-block'});
	}
}

setTimeout('changeSlide()', slideInterval);

function changeSlide(){
	if(!isSlide) return;
	$('.slider-'+currSlide).fadeOut(slideDelay, function(){
		currSlide += 1;
		if(currSlide>2) currSlide = 0;
		$('.slider-'+currSlide).fadeIn(slideDelay);
		$('.point').removeClass('selected');
		$('.point-'+currSlide).addClass('selected');
	});
	if(isSlide) setTimeout('changeSlide()', slideInterval);
}